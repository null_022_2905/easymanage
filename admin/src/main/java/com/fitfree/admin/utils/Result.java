package com.fitfree.admin.utils;

import com.fitfree.admin.enums.ResultCode;
import lombok.Data;

import java.io.Serializable;

/***
 com.fitfree.admin.utils
 hanzl
 2020/4/25
 18:02
 返回结果的实体类
 **/
@Data
public class Result implements Serializable {
    private Integer code;
    private String message;
    private Object data;
    public  Result(Integer code,String message,Object data){
        this.code=code;
        this.message=message;
        this.data=data;
    }
    public Result(){}
    public static Result success(Object data){
        Result result=new Result();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMessage(ResultCode.SUCCESS.getMesage());
        result.setData(data);
        return result;
    }
    public static Result failure(){
        Result result=new Result();
        result.setCode(ResultCode.FAILURE.getCode());
        result.setMessage(ResultCode.FAILURE.getMesage());
        result.setData(null);
        return result;
    }
}
