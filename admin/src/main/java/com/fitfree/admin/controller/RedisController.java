package com.fitfree.admin.controller;

import com.fitfree.admin.enums.RedisInfoProperties;
import com.fitfree.admin.model.system.SysMenu;
import com.fitfree.admin.utils.Meta;
import com.fitfree.admin.utils.RedisUtil;
import com.fitfree.admin.utils.RespoBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Properties;

/***
 com.fitfree.admin.controller
 hanzl
 2020/4/19
 21:27
 **/
@RestController
@RequestMapping("/redis")
public class RedisController {
 @Resource
 private RedisUtil redisUtil;
    @RequestMapping("/redisInfo")
    public RespoBody redisInfo() {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        Properties redisInfo=redisUtil.getRedisInfo(RedisInfoProperties.stats);
        return new RespoBody(redisInfo, meta);
    }
}
