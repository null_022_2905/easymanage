package com.fitfree.admin.utils;

import lombok.Data;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/3
 * Time:9:54
 */
@Data
public class Meta {
    private String msg;
    private int status;
    private int count;
}
