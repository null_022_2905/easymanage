package com.fitfree.admin.model;

import lombok.Data;

import java.util.Date;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/28
 * Time:8:01
 */
@Data
public class Order {
    /***
     * 订单id
     */
    private long id;
    /***
     * 订单id
     */
    private String orderNo;
    /***
     * 是否支付
     */
    private boolean ifPay;
    /**
     * 支付金额
     */
    private float payment;
    /***
     * 下单时间
     */
    private Date createTime;
}
