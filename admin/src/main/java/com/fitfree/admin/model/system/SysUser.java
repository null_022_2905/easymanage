package com.fitfree.admin.model.system;

import lombok.Data;

import java.util.Date;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/9
 * Time:11:52
 */
@Data
public class SysUser {
    /***
     * 用户Id
     */
    private long userId;
    /***
     * 用户名称
     */
    private String username;
    /***
     * 密码
     */
    private String password;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 手机号
     */
    private String mobile;
    /***
     * 盐
     */
    private String salt;
    /**
     * 状态 状态  0：禁用   1：正常
     */
    private boolean status;
    /**
     * 部门id
     */
    private long deptId;
    /***
     * 创建时间
     */
    private Date createTime;
    /***
     * 用户昵称
     */
    private String nickname;
    /***
     */
    private String token;
    /***
     *  角色名称
     */
    private String roleNames;
    /***
     * 角色Id
     */
    private String roleIds;
}
