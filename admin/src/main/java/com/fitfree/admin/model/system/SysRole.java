package com.fitfree.admin.model.system;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/9
 * Time:12:25
 */
@Data
public class SysRole {
    /***
     *  角色Id
     */
    private long roleId;
    /***
     * 角色名称
     */
    private String roleName;
    /***
     * 备注
     */
    private String remark;
    /***
     * 部门Id
     */
    private long deptId;
    /***
     * 创建时间
     */
    private Date createTime;
    /***
     * 关联菜单
     */
    private List<SysMenu> children;

    /***
     * id 字符串
     */
    private String idStr;
}
