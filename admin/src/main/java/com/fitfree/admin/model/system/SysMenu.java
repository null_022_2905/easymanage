package com.fitfree.admin.model.system;

import lombok.Data;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/9
 * Time:12:02
 */
@Data
public class SysMenu {
    /**
     * 菜单id
     */
  private long  menuId;
    /***
     * 父菜单id
     */
  private long  parentId;
    /***
     * 菜单名称
     */
  private String  name;
    /***
     * 菜单url
     */
  private String url;
    /***
     * 菜单权限
     */
  private String  perms;
    /***
     * 菜单类型
     */
  private int type;
    /***
     * 菜单图标
     */
  private String icon;
    /***
     * 排序
     */
  private int orderNum;
  /**
   * 状态
   */
  private int status;
  /***
   * 子菜单
   */
  private List<SysMenu> children;
}
