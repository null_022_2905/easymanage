package com.fitfree.admin.enums;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/8
 * Time:10:46
 */
@ConfigurationProperties(prefix = "token")
@Data
public class JwtProperties {
    //token过期时间，单位分钟
   public  Integer tokenExpireTime;
    //刷新Token过期时间，单位分钟
   public  Integer refreshTokenExpireTime;
    //Shiro缓存有效期，单位分钟
    public Integer shiroCacheExpireTime;
    //token加密密钥
   public  String secretKey;
}
