package com.fitfree.admin.service.system;

import com.fitfree.admin.dao.system.SysMenuDao;
import com.fitfree.admin.model.system.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/9
 * Time:22:12
 */
@Service
public class SysMenuService {
    @Autowired
    private SysMenuDao sysMenuDao;

   public  List<SysMenu> getAllMenus(int pageNo, int pageSize){
       if(pageNo==0){
           pageNo=1;
       }
       if(pageSize==0){
           pageSize=10;
       }
       pageNo=(pageNo-1)*pageSize;
       return sysMenuDao.getAllMenus(pageNo, pageSize);
   }

    /***
     * 根据角色Id和菜单Id删除关系
     * @param roleId
     * @param menuId
     * @return
     */
  public int deleteMrBymenuIdAndRoleId(int roleId,int menuId){
      return sysMenuDao.deleteMrBymenuIdAndRoleId(roleId, menuId);
  }

    /***
     * 查询菜单列表
     * @param pageNo
     * @param pageSize
     * @param query
     * @return
     */
    public  List<SysMenu> getAllMenusByQuery(String query,int pageNo, int pageSize){
        if(pageNo==0){
            pageNo=1;
        }
        if(pageSize==0){
            pageSize=10;
        }
        pageNo=(pageNo-1)*pageSize;
        return sysMenuDao.getAllMenusByQuery(query,pageNo, pageSize);
    }
    /**
     * 查询所有的子菜单
     * @return
     */
    public  List<SysMenu> getMunuList(){
        List<SysMenu> parent=sysMenuDao.getMenuListByParentId(0);
        if(parent!=null&&parent.size()>0){
            for(SysMenu sysMenu:parent){
                List<SysMenu> children=sysMenuDao.getMenuListByParentId(sysMenu.getMenuId());
                sysMenu.setChildren(children);
            }
        }
        return parent;
    }

    /*
    保存菜单信息
     * @return
     */
    public  int saveMenu(SysMenu sysMenu){
       return sysMenuDao.saveMenu(sysMenu);
    }
    /**
     * 根据菜单Id获取菜单信息
     * @param menuId
     * @return
     */
    public SysMenu getUMenusById(long menuId){
        return sysMenuDao.getUMenusById(menuId);
    }

    /***
     * 更新菜单信息
     * @param sysMenu
     * @return
     */
    public int updateMenu(SysMenu sysMenu){
        return sysMenuDao.updateMenu(sysMenu);
    }
}
