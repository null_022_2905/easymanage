package com.fitfree.admin.dao;

import com.fitfree.admin.model.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/12
 * Time:15:38
 */
@Mapper
public interface CategoryDao {
    /***
     * 根据父Id查询分类
     * @param parentId
     * @return
     */
    List<Category> queryCateGoryListByparentId(long parentId);

    /***
     * 保存分类
     * @param category
     * @return
     */
    int saveCategory(Category category);
}
