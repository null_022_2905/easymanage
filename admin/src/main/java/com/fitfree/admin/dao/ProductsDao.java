package com.fitfree.admin.dao;

import com.fitfree.admin.model.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/16
 * Time:21:18
 */
@Mapper
public interface ProductsDao {
    /***
     * 根据条件查询信息
     * @param query
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<Product> getProductList(@Param("query") String query, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

    /***
     * g根据条件查询商品数量
     * @param query
     * @return
     */
    int selectProductCountByParams(@Param("query") String query);

    /***
     * 添加商品
     * @param product
     * @return
     */
    int addProduct(Product product);

    /***
     * 根据id删除数据
     * @param id
     * @return
     */
    int deleteById(long id);
}
