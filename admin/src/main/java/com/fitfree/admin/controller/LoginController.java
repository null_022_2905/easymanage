package com.fitfree.admin.controller;

import com.fitfree.admin.model.system.SysUser;
import com.fitfree.admin.service.system.SysUserService;
import com.fitfree.admin.utils.jwt.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/2
 * Time:15:35
 */
@RestController
public class LoginController {
    @Autowired
    private SysUserService  sysUserService;
    @PostMapping("/login")
    @CrossOrigin(value="http://localhost:8083",maxAge = 1800,allowedHeaders = "*")
    public SysUser login(@RequestBody SysUser user){
        SysUser sysUser=sysUserService.findUserByUserName(user.getUsername());
        if(sysUser!=null&&user.getPassword().equals(sysUser.getPassword())){
            user.setToken(JwtUtil.sign(sysUser.getUserId(),user.getPassword()));
        }
        //todo 异常处理
        return user;
    }
}
