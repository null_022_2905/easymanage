package com.fitfree.admin.dao.system;
import com.fitfree.admin.model.system.SysRole;
import com.fitfree.admin.model.system.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/9
 * Time:12:14
 */
@Mapper
public interface SysUserDao {
    /***
     * 根据用户名称查询用户
     * @param userName
     * @return
     */
    SysUser findUserByUserName(String userName);

    /***
     * 根据用户Id查询
     * @param userId
     * @return
     */
    SysUser findUserByUserId(long userId);
    /***
     * 根据用户Id查询角色
     * @param userId
     * @return
     */
    List<SysRole> getUserRolesByUid(long userId);

    /***
     * 添加用户
     * @param user
     * @return
     */
    int addUser(SysUser user);

    /**
     * 跟据条件查询用户列表
     * @param pageNo
     * @param pageSize
     * @param username
     * @return
     */
    List<SysUser> selectUserByParams(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize, @Param("username") String username);

    /***
     * 根据条件查询用户数量
     * @param username
     * @return
     */
    int selectUserCountByParams(@Param("username") String username);
    /***
     * 修改用户
     * @param user
     * @return
     */
    int editUser(SysUser user);

    /***
     * 根据id删除用户
     */
    int deleteUserByid(long userId);

    /***
     * 获得所有的角色
     * @return
     */
    List<SysRole> getRoLeList();

    /***
     * 根据用户ID查询角色信息
     * @param userId
     * @return
     */
    SysRole getRolesByUserId(long userId);

    /***
     * 根据用户Id和角色Id查询关系表中是否有记录
     * @param userId
     * @param roleId
     * @return
     */
    int queryNumByUserIdAndroleId(@Param("userId") long userId, @Param("roleId") long roleId);

    /***
     * 向sys_user_role 插入关系记录
     * @param userId
     * @param roleId
     * @return
     */
    int addUrInfo(@Param("userId") long userId, @Param("roleId") long roleId);



    /***
     * 向sys_user_role 更新用户状态
     * @param userId
     * @param status
     * @return
     */
    int updateUserSattus(@Param("userId") long userId, @Param("status") int  status);

    /***
     * 保存角色基本信息
     * @param sysRole
     * @return
     */
    int saveRole(SysRole sysRole);

    /***
     * 根据角色Id获取角色信息
     * @param roleId
     * @return
     */
    SysRole getRoleById(@Param("roleId") long roleId);


    /***
     * 修改角色
     * @param sysRole
     * @return
     */
    int editRole(SysRole sysRole);

    /****
     * 查询角色和菜单是否已经绑定到了一起
     * @param roleId
     * @param menuId
     * @return
     */
    int queryCountByRoleAndMenuId(@Param("roleId") int roleId,@Param("menuId") int menuId);

    /***
     * 保存绑定关系
     * @param roleId
     * @param menuId
     * @return
     */
    int saveRoleManu(@Param("roleId") int roleId,@Param("menuId") int menuId);

    /***
     * 根据roleId删除
     * @param roleId
     * @return
     */
    int deleteRoleMenuByRoleId(@Param("roleId") int roleId);
}
