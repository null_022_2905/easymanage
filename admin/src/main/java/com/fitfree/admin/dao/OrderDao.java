package com.fitfree.admin.dao;

import com.fitfree.admin.model.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/28
 * Time:8:08
 */
@Mapper
public interface OrderDao {
    /***
     * 根据条件查询信息
     * @param query
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<Order> getOrderList(@Param("query") String query, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

    /***
     * g根据条件查询订单数量
     * @param query
     * @return
     */
    int selectOrdersCountByParams(@Param("query") String query);
}
