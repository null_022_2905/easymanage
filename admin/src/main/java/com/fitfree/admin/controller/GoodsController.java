package com.fitfree.admin.controller;

import com.fitfree.admin.model.Product;
import com.fitfree.admin.service.ProductService;
import com.fitfree.admin.utils.Meta;
import com.fitfree.admin.utils.RespoBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/16
 * Time:21:29
 */
@RestController
@RequestMapping("/goods")
public class GoodsController extends BasicController {
    @Autowired
    private ProductService productService;
    @RequestMapping("/getProductList")
    public RespoBody getProductList(@RequestParam("pageNo") int pageNo,@RequestParam("pageSize") int pageSize,
                                    @RequestParam("query") String  query ){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        List<Product> products=productService.getProductList(query, pageNo, pageSize);
        int count=productService.selectProductCountByParams(query);
        meta.setCount(count);
        return new RespoBody(products,meta);
    }

    /***
     * 根据id删除商品
     * @param id
     * @return
     */
    @RequestMapping("/deleteById")
    public RespoBody deleteById(@RequestParam(value = "id" ) long id ){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        int i=productService.deleteById(id);
        return new RespoBody(i,meta);
    }

    /***
     * 根据id删除商品
     * @return
     */
    @PostMapping("/addProduct")
    public RespoBody addProduct(@RequestBody Product product){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("添加商品成功");
        int i=productService.addProduct(product);
        return new RespoBody(i,meta);
    }


}
