package com.fitfree.admin.model.drools;

import lombok.Data;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/3/19
 * Time:9:58
 */
@Data
public class Person {
    private String name ;
    private Integer age ;

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
}
