package com.fitfree.admin.model;

import lombok.Data;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/12
 * Time:15:07
 */
@Data
public class Category {
    /***
     *分类Id
     */
    private long categoryId;
    /**
     * 父Id
     */
    private long  parentId;
    /***
     * 分类名称
     */
    private String categoryName;
    /***
     * 是否有效
     */
    private boolean ifValid;

    /***
     * 当前的等级
     */
    private int categoryLevel;

    private List<Category> children;
}
