package com.fitfree.admin;

import com.fitfree.admin.utils.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminApplicationTests {
@Autowired
private RedisUtil redisUtil;
    @Test
    public void contextLoads() {
    }
    @Test
    public void test(){
       String aa= redisUtil.getRedisInfo();
       System.out.println(aa);
    }

}
