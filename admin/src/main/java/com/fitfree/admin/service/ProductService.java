package com.fitfree.admin.service;

import com.fitfree.admin.dao.ProductsDao;
import com.fitfree.admin.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/16
 * Time:21:31
 */
@Service
public class ProductService {
    @Autowired
    private ProductsDao productsDao;

    /*****
     * g根据条件查询商品信息
     * @param query
     * @param pageNo
     * @param pageSize
     * @return
     */
    public  List<Product> getProductList(String query,int pageNo,int pageSize){
        if (pageNo == 0) {
            pageNo = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        pageNo = (pageNo - 1) * pageSize;
        return productsDao.getProductList(query, pageNo, pageSize);
    }

    /**
     * 查询商品数量
     * @param query
     * @return
     */
    public int selectProductCountByParams(String query){
        return productsDao.selectProductCountByParams(query);
    }

    /**
     * 添加商品
     * @param product
     * @return
     */
    public int addProduct(Product product){
        return productsDao.addProduct(product);
    }

    /***
     * @param id
     * @return
     */
    public int deleteById(long id){
        return productsDao.deleteById(id);
    }
}
