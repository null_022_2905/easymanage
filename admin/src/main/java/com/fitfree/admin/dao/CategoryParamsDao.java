package com.fitfree.admin.dao;

import com.fitfree.admin.model.CategoryParams;
import com.fitfree.admin.model.CategoryParamsValue;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/15
 * Time:12:50
 */
@Mapper
public interface CategoryParamsDao {
    List<CategoryParams> getCateGoryParamListByType(@Param("type") int type, @Param("categoryId") int categoryId);

    /***
     * 保存参数
     * @param categoryParams
     * @return
     */
    int saveParams(CategoryParams categoryParams);

    /***
     * 根据id更新数据
     * @param categoryParams
     * @return
     */
    int updateCateGoryPrams(CategoryParams categoryParams);

    /***
     * 根据id查询参数信息
     * @param id
     * @return
     */
    CategoryParams getCateGoryParamById(int id);

    /***
     * 根据id删除数据
     * @param id
     * @return
     */
   int  deleteCategoryById(int id);

    /***
     * 根据属性Id查询属性值
     * @param categoryParamsId
     * @return
     */
    List<CategoryParamsValue> getCateGoryParamValues(long categoryParamsId);

    /***
     * 根据id更新属性值
     * @param id
     * @param paramsValue
     * @return
     */
   int  updateCateGoryPramsValues(@Param("id") int id, @Param("paramsValue") String paramsValue);
}
