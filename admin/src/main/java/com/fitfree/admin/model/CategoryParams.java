package com.fitfree.admin.model;

import lombok.Data;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/15
 * Time:12:51
 */
@Data
public class CategoryParams {
    private Long id;
    /***
     * 分类id
     */
    private long categoryId;
    /**
     * 参数名称
     */
    private String paramsName;
    /***
     * 是否有效 1.动态，2静态
     */
    private int type;

    private String strtype;

    private String  paramsValue;

}
