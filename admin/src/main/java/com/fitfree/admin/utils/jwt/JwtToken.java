package com.fitfree.admin.utils.jwt;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/7
 * Time:22:01
 */
public class JwtToken implements AuthenticationToken {

    private String token;

    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
