package com.fitfree.admin.service;

import com.fitfree.admin.dao.CategoryParamsDao;
import com.fitfree.admin.model.CategoryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/15
 * Time:12:58
 */
@Service
public class CategoryParamsService {
    @Autowired
    private CategoryParamsDao categoryParamsDao;

   public  List<CategoryParams> getCateGoryParamListByType(String strtype,int categoryId){
       int type=0;
       if("diagnizeParam".equals(strtype)){
           type=1;
       }
       if("staticParam".equals(strtype)){
           type=2;
       }
       List<CategoryParams> result=categoryParamsDao.getCateGoryParamListByType(type,categoryId);
      /* if(result!=null&&result.size()>0){
           result.forEach(item->{
               List<CategoryParamsValue> categoryParamsValues=categoryParamsDao.getCateGoryParamValues(item.getId());
               item.setCategoryValues(categoryParamsValues);
           });
       }*/
       return result;
   }

   public int saveParams(CategoryParams categoryParams){
       int type=0;
       if("diagnizeParam".equals(categoryParams.getStrtype())){
           type=1;
       }
       if("staticParam".equals(categoryParams.getStrtype())){
           type=2;
       }
       categoryParams.setType(type);
       if(categoryParams.getId()!=null){
           return categoryParamsDao.updateCateGoryPrams(categoryParams);
       }
       return categoryParamsDao.saveParams(categoryParams);
   }
    public CategoryParams getCateGoryParamById(int id){
        return categoryParamsDao.getCateGoryParamById(id);
    }

    public int  deleteCategoryById(int id){
        return categoryParamsDao.deleteCategoryById(id);
    }

    /**
     * 根据id更新属性值
     * @param id
     * @param paramsValue
     * @return
     */
   public int  updateCateGoryPramsValues(int id,String paramsValue){
       return categoryParamsDao.updateCateGoryPramsValues(id, paramsValue);
   }
}
