package com.fitfree.admin.controller;

import com.fitfree.admin.model.Location;
import com.fitfree.admin.model.Order;
import com.fitfree.admin.service.OrderService;
import com.fitfree.admin.utils.Meta;
import com.fitfree.admin.utils.RespoBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/28
 * Time:8:26
 */
@RestController
@RequestMapping("/orders")
public class OrdersController {
    @Autowired
    private OrderService orderService;
    @RequestMapping("/getOrderList")
    public RespoBody getProductList(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize,
                                    @RequestParam("query") String  query ){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        List<Order> orders=orderService.getOrderList(query, pageNo, pageSize);
        int count=orderService.selectOrdersCountByParams(query);
        meta.setCount(count);
        return new RespoBody(orders,meta);
    }
    @RequestMapping("/getProductLocationList")
    public RespoBody getProductLocationList(){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
       List<Location> locations=new ArrayList<>();
        Location l1=new Location();
        l1.setTime(new Date());
        l1.setFtime(new Date());
        l1.setContext("物流已到达河北省魏县");
        l1.setLocation("");
        locations.add(l1);
        Location l2=new Location();
        l2.setTime(new Date());
        l2.setFtime(new Date());
        l2.setContext("物流已到达河北省邯郸");
        l2.setLocation("");
        locations.add(l2);
        return new RespoBody(locations,meta);
    }
}
