package com.fitfree.admin.model;

import lombok.Data;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/16
 * Time:9:01
 */
@Data
public class CategoryParamsValue {
    private Long id;
    //属性Id
    private Long categoryParamsId;
    //属性值
    private String paramsValue;
}
