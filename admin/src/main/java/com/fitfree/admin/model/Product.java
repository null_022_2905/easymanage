package com.fitfree.admin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/16
 * Time:21:11
 */
@Data
public class Product {
    private long id;
    /***
     * 分类ID
     */
    private long categoryId;
    /***
     * 商品名称
     */
    private String productName;
    /***
     * 商品数量
     */
    private int productNum;
    /***
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /***
     * 商品价格
     */
    private float productPrice;
    /***
     * 商品图片地址
     */
    private String productImageurl;
    /***
     * 商品详情
     */
    private String productDesc;
    /***
     * 商品参数
     */
    private String productParams;
}
