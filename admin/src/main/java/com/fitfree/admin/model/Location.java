package com.fitfree.admin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/29
 * Time:8:27
 */
@Data
public class Location {
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date time;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date ftime;
    private String context;
    private String location;
}
