package com.fitfree.admin.system.model;

import lombok.Data;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/7
 * Time:22:17
 */
@Data
public class SysUser {
    private String username;
    private String password;
}
