package com.fitfree.admin.service;

import com.fitfree.admin.dao.CategoryDao;
import com.fitfree.admin.model.Category;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/12
 * Time:15:34
 */
@Service
public class CategoryService {
    @Resource
    private CategoryDao categoryDao;

    /***
     *  type 1,2,3 表示一次性显示几级
     * @return
     */
    public List<Category> getCateGoryList(int type) {
        List<Category> result = categoryDao.queryCateGoryListByparentId(0);
        if (type == 2||type == 3) {
            if (result != null && result.size() > 0) {
                for (Category pa : result) {
                    List<Category> children = categoryDao.queryCateGoryListByparentId(pa.getCategoryId());
                    pa.setChildren(children);
                    if (type == 3) {
                        if (children != null && children.size() > 0) {
                            for (Category category : children) {
                                List<Category> grandson = categoryDao.queryCateGoryListByparentId(category.getCategoryId());
                                category.setChildren(grandson);
                            }
                        }
                    }
                }
            }

        }
        return result;
    }

    /***
     * @param category
     * @return
     */
    public int saveCategory(Category category){
        return categoryDao.saveCategory(category);
    }
}
