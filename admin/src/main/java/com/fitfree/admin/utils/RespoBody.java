package com.fitfree.admin.utils;

import lombok.Data;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/3
 * Time:9:54
 */
@Data
public class RespoBody {
    private Meta meta;
    private Object data;
    public RespoBody(Object data,Meta meta){
        this.meta=meta;
        this.data=data;
    }
}
