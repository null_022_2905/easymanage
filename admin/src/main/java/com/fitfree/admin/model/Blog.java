package com.fitfree.admin.model;

import lombok.Data;

/***
 com.fitfree.admin.model
 hanzl
 2020/5/1
 17:16
 **/
@Data
public class Blog {
    private long id;
    private String title;
    private String content;
}
