package com.fitfree.easymanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasymanageApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasymanageApplication.class, args);
    }

}
