package com.fitfree.admin.service;

import com.fitfree.admin.dao.BlogDao;
import com.fitfree.admin.model.Blog;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/***
 com.fitfree.admin.service
 hanzl
 2020/5/1
 17:34
 **/
@Service
public class BlogService {
    @Resource
    private BlogDao blogDao;

    public List<Blog> queryList() {
        return blogDao.queryList();
    }

    public int insert(Blog blog) {
        return blogDao.insert(blog);
    }

}
