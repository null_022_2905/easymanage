package com.fitfree.admin.controller;

import com.fitfree.admin.model.Category;
import com.fitfree.admin.model.CategoryParams;
import com.fitfree.admin.service.CategoryParamsService;
import com.fitfree.admin.service.CategoryService;
import com.fitfree.admin.utils.Meta;
import com.fitfree.admin.utils.RespoBody;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/12
 * Time:20:18
 * 分类controller
 */
@RestController
@RequestMapping("/category")
public class CategoryController extends BasicController{
    @Resource
    private CategoryService categoryService;
    @Resource
    private CategoryParamsService categoryParamsService;
    @RequestMapping("/getCateGoryList")
    public RespoBody getCateGoryList(@RequestParam(value = "type",defaultValue = "3") int type){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        List<Category> result=categoryService.getCateGoryList(type);
        return new RespoBody(result,meta);
    }
    @PostMapping("/saveCategory")
    public RespoBody saveCategory(@RequestBody Category category){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        int i=  categoryService.saveCategory(category);
        return new RespoBody(i,meta);
    }

    @RequestMapping("/getCateGoryParamsList")
    public RespoBody getCateGoryParamsList(@RequestParam("type") String type,@RequestParam("categoryId") int categoryId){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        List<CategoryParams> categoryParamses=categoryParamsService.getCateGoryParamListByType(type,categoryId);
        return new RespoBody(categoryParamses,meta);
    }

    @PostMapping("/saveCateGoryParams")
    public RespoBody saveCateGoryParams(@RequestBody CategoryParams categoryParams){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        int i=categoryParamsService.saveParams(categoryParams);
        return new RespoBody(i,meta);
    }

    /**
     * 根据id查询分类属性信息
     * @param id
     * @return
     */
    @RequestMapping("/getCateGoryParamsById")
    public RespoBody getCateGoryParamsById(@RequestParam("id") int id){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        CategoryParams categoryParams=categoryParamsService.getCateGoryParamById(id);
        return new RespoBody(categoryParams,meta);
    }

    /**
     * g根据id删除数据
     * @param id
     * @return
     */
    @RequestMapping("/deleteCategoryById")
    public RespoBody deleteCategoryById(@RequestParam("id") int id){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        int i=categoryParamsService.deleteCategoryById(id);
        return new RespoBody(i,meta);
    }

    @RequestMapping("/updateParamsValues")
    public RespoBody updateParamsValues(@RequestParam("id") int id,@RequestParam("paramsValues") String ParamsValues){
        Meta meta=new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        int i=categoryParamsService.updateCateGoryPramsValues(id, ParamsValues);
        return new RespoBody(i,meta);
    }

}
