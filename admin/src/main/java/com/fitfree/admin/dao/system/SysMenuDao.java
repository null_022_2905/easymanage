package com.fitfree.admin.dao.system;

import com.fitfree.admin.model.system.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/9
 * Time:12:14
 */
@Mapper
public interface SysMenuDao {
    /***
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<SysMenu>  getAllMenus(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

    /****
     * 获取角色列表
     * @return
     */
    List<SysMenu> getMenusByRoleIDAndAPrentId(@Param("roleId") long roleId, @Param("parentId") long parentId);

    /***
     * 根据用户id和父Id查询菜单
     * @param userId
     * @param parentId
     * @return
     */
    List<SysMenu>   getMenusByParentId(@Param("userId") long userId, @Param("parentId") long parentId);

    /**
     * 根据角色Id和菜单Id删除关系
     * @param roleId
     * @param menuId
     * @return
     */
    int deleteMrBymenuIdAndRoleId(@Param("roleId") int roleId, @Param("menuId") int menuId);

    /***
     * 查询菜单列表
     * @param query
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<SysMenu> getAllMenusByQuery(@Param("query") String query, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

    /***
     * 根据父Id查询子菜单
     * @param parentId
     * @return
     */
    List<SysMenu> getMenuListByParentId(@Param("parentId") long parentId);

    /**
     * 保存菜单信息
     * @param sysMenu
     * @return
     */
    int saveMenu(SysMenu sysMenu);

    SysMenu getUMenusById(@Param("menuId") long menuId);

    /***
     * 更新菜单信息
     * @param sysMenu
     * @return
     */
    int updateMenu(SysMenu sysMenu);
}
