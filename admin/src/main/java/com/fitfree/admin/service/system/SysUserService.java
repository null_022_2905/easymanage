package com.fitfree.admin.service.system;

import com.fitfree.admin.dao.system.SysMenuDao;
import com.fitfree.admin.dao.system.SysUserDao;
import com.fitfree.admin.model.system.SysMenu;
import com.fitfree.admin.model.system.SysRole;
import com.fitfree.admin.model.system.SysUser;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/9
 * Time:12:15
 */
@Service
public class SysUserService {
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private SysMenuDao sysMenuDao;

    public SysUser findUserByUserName(String userName) {
        SysUser sysUser = sysUserDao.findUserByUserName(userName);
        return sysUser;
    }

    public SysUser findUserByUserId(long userId) {
        SysUser sysUser = sysUserDao.findUserByUserId(userId);
        return sysUser;
    }

    public List<SysMenu> getMenusByUserId(long userId, int type) {
        List<SysMenu> parent = sysMenuDao.getMenusByParentId(userId, 0);
        for (SysMenu pa : parent) {
            List<SysMenu> children = sysMenuDao.getMenusByParentId(userId, pa.getMenuId());
            pa.setChildren(children);
            if (type == 1) {
                for (SysMenu ch : children) {
                    List<SysMenu> children2 = sysMenuDao.getMenusByParentId(userId, ch.getMenuId());
                    ch.setChildren(children2);
                }
            }
        }
        return parent;
    }

    /***
     * 根据条件查询用户信息
     * @param pageNo
     * @param pageSize
     * @param username
     * @return
     */
    public List<SysUser> selectUserByParams(int pageNo, int pageSize, String username) {
        if (pageNo == 0) {
            pageNo = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        pageNo = (pageNo - 1) * pageSize;
        List<SysUser> sysUsers = sysUserDao.selectUserByParams(pageNo, pageSize, username);
        if (sysUsers != null && sysUsers.size() > 0) {
            for (SysUser sysUser : sysUsers) {
                SysRole sysRole = sysUserDao.getRolesByUserId(sysUser.getUserId());
                if (sysRole != null) {
                    sysUser.setRoleNames(sysRole.getRoleName());
                    sysUser.setRoleIds(sysRole.getIdStr());
                }
            }
        }
        return sysUsers;
    }

    /***
     * 查询用户数量
     * @param username
     * @return
     */
    public int selectUserCountByParams(String username) {
        return sysUserDao.selectUserCountByParams(username);
    }

    /***
     * 添加用户
     */
    public int addUser(SysUser user) {
        return sysUserDao.addUser(user);
    }

    /***
     * 修改用户信息
     */
    public int editUser(SysUser user) {
        return sysUserDao.editUser(user);
    }

    /**
     * 删除用户
     *
     * @param userId
     * @return
     */
    public int deleteUserByid(long userId) {
        return sysUserDao.deleteUserByid(userId);
    }

    /***
     * 获得所有的角色
     * @return
     */
    public List<SysRole> getRoLeList() {
        List<SysRole> sysRoles = sysUserDao.getRoLeList();
        if (sysRoles != null && sysRoles.size() > 0) {
            for (SysRole sysRole : sysRoles) {
                List<SysMenu> sysMenusparent = sysMenuDao.getMenusByRoleIDAndAPrentId(sysRole.getRoleId(), 0);
                sysRole.setChildren(sysMenusparent);
                for (SysMenu children : sysMenusparent) {
                    List<SysMenu> children2 = sysMenuDao.getMenusByRoleIDAndAPrentId(sysRole.getRoleId(), children.getMenuId());
                    children.setChildren(children2);
                    for (SysMenu chil2 : children2) {
                        List<SysMenu> children4 = sysMenuDao.getMenusByRoleIDAndAPrentId(sysRole.getRoleId(), chil2.getMenuId());
                        chil2.setChildren(children4);
                    }
                }
            }
        }
        return sysRoles;
    }

    /***
     * 根据角色Id查询菜单
     * @param roleId
     * @return
     */
    public List<SysMenu> getRoLeListByRoleId(long roleId) {
        List<SysMenu> sysMenusparent = sysMenuDao.getMenusByRoleIDAndAPrentId(roleId, 0);
        for (SysMenu children : sysMenusparent) {
            List<SysMenu> children2 = sysMenuDao.getMenusByRoleIDAndAPrentId(roleId, children.getMenuId());
            children.setChildren(children2);
            for (SysMenu chil2 : children2) {
                List<SysMenu> children4 = sysMenuDao.getMenusByRoleIDAndAPrentId(roleId, chil2.getMenuId());
                chil2.setChildren(children4);
            }
        }
        return sysMenusparent;
    }

    /**
     * 保存角色信息
     *
     * @param userId
     * @param roleId
     */
    public void saveRoleInfo(long userId, long roleId) {
        int i = sysUserDao.queryNumByUserIdAndroleId(userId, roleId);
        if (i == 0) {
            sysUserDao.addUrInfo(userId, roleId);
        }
    }

    /**
     * 更新用户状态可用，非可用
     *
     * @param userId
     * @param status
     */
    public void updateUserStatus(long userId, int status) {
        int i = sysUserDao.updateUserSattus(userId, status);
    }


    /***
     * 添加角色
     */
    public int saveRole(SysRole sysRole) {
        return sysUserDao.saveRole(sysRole);
    }

    /**
     * 根据角色Id查询角色信息
     *
     * @param roleId
     * @return
     */
    public SysRole findRoleById(long roleId) {
        SysRole sysRole = sysUserDao.getRoleById(roleId);
        return sysRole;
    }


    /***
     * 修改用户信息
     */
    public int editRole(SysRole sysRole) {
        return sysUserDao.editRole(sysRole);
    }

    /**
     * 保存角色和菜单的关系
     *
     * @param roleId
     * @param menuIds
     * @return
     */
    public int saveRoleMenu(int roleId, String menuIds) {
        sysUserDao.deleteRoleMenuByRoleId(roleId);
        if (StringUtils.isNotBlank(menuIds)) {
            for (String menuIdStr : menuIds.split(",")) {
                int menuId = Integer.parseInt(menuIdStr);
                sysUserDao.saveRoleManu(roleId, menuId);
            }
        }
        return 1;
    }

}
