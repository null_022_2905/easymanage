package com.fitfree.admin.enums;


import lombok.Data;

/***
 com.fitfree.admin.enums
 hanzl
 2020/4/25
 17:37
 **/
public enum ResultCode {
    SUCCESS(200,"成功"),
    FAILURE(500,"失败"),
    PARAM_IS_INVALID(1001,"参数无效"),
    PARAM_IS_BLANK(1002,"参数为空"),
    PARAM_TYPE_BIND_ERROR(1003,"参数类型错误"),
    PARAM_NOT_COMPLETE(1004,"参数缺失"),
    USER_NOT_LOGGED_IN(2001,"用户未登陆"),
    USER_LOGIN_ERROR(2002,"账号不存在或者密码错误"),
    USER_ACCOUNT_FORBBIDDEN(2003,"账号已被禁用"),
    USER_NOT_EXIST(2004,"用户不存在"),
    USER_HAS_EXISTED(2005,"用户已存在");
    private Integer code;
    private String message;
    private ResultCode(Integer code,String message){
        this.code=code;
        this.message=message;
    }
    public Integer getCode(){
        return this.code;
    }
    public void setCode(Integer code){
        this.code=code;
    }
    public String  getMesage(){
        return this.message;
    }
    public void setMessage(String message){
        this.message=message;
    }
}
