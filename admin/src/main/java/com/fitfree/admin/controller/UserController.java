package com.fitfree.admin.controller;

import com.fitfree.admin.model.Product;
import com.fitfree.admin.model.system.SysMenu;
import com.fitfree.admin.model.system.SysRole;
import com.fitfree.admin.model.system.SysUser;
import com.fitfree.admin.service.system.SysMenuService;
import com.fitfree.admin.service.system.SysUserService;
import com.fitfree.admin.utils.Meta;
import com.fitfree.admin.utils.RespoBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/2
 * Time:15:34
 */
@RestController
@RequestMapping("/user")
public class UserController extends  BasicController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysMenuService sysMenuService;

    @RequestMapping("/itemList")
    public RespoBody getItemList() {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        long userId = this.getUserIdFromToken();
        List<SysMenu> menus = sysUserService.getMenusByUserId(userId, 0);
        return new RespoBody(menus, meta);
    }

    @RequestMapping("/getAllItemList")
    public RespoBody getAllItemList() {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        long userId = this.getUserIdFromToken();
        List<SysMenu> menus = sysUserService.getMenusByUserId(0, 1);
        return new RespoBody(menus, meta);
    }

    @RequestMapping("/userList")
    @CrossOrigin(value = "http://localhost:8080", maxAge = 1800, allowedHeaders = "*")
    public RespoBody getUserList(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize,
                                 @RequestParam("query") String query) {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        List<SysUser> userList = sysUserService.selectUserByParams(pageNo, pageSize, query);
        meta.setCount(sysUserService.selectUserCountByParams(query));
        return new RespoBody(userList, meta);
    }

    @PostMapping("/saveUser")
    @CrossOrigin(value = "http://localhost:8080", maxAge = 1800, allowedHeaders = "*")
    public RespoBody saveUser(@RequestBody SysUser user) {
        int i = sysUserService.addUser(user);
        Meta meta = new Meta();
        if (i > 0) {
            meta.setStatus(200);
            meta.setMsg("成功");
        } else {
            meta.setStatus(401);
            meta.setMsg("添加失败");
        }
        return new RespoBody(null, meta);
    }

    @PostMapping("/editUser")
    @CrossOrigin(value = "http://localhost:8080", maxAge = 1800, allowedHeaders = "*")
    public RespoBody editUser(@RequestBody SysUser user) {
        int i = sysUserService.editUser(user);
        Meta meta = new Meta();
        if (i > 0) {
            meta.setStatus(200);
            meta.setMsg("成功");
        } else {
            meta.setStatus(401);
            meta.setMsg("修改失败");
        }
        return new RespoBody(null, meta);
    }

    @RequestMapping("/getUserById")
    @CrossOrigin(value = "http://localhost:8080", maxAge = 1800, allowedHeaders = "*")
    public RespoBody getUserById(@RequestParam("id") int id) {
        SysUser user = sysUserService.findUserByUserId(id);
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        return new RespoBody(user, meta);
    }

    @RequestMapping("/deleteUserById")
    @CrossOrigin(value = "http://localhost:8080", maxAge = 1800, allowedHeaders = "*")
    public RespoBody deleteUserById(@RequestParam("id") int id) {
        int i = sysUserService.deleteUserByid(id);
        Meta meta = new Meta();
        if (i == 0) {
            meta.setStatus(200);
            meta.setMsg("成功");
        } else {
            meta.setStatus(401);
            meta.setMsg("删除失败");
        }
        return new RespoBody(null, meta);
    }

    @RequestMapping("/getRightList")
    @CrossOrigin(value = "http://localhost:8080", maxAge = 1800, allowedHeaders = "*")
    public RespoBody getRightList() {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        List<SysMenu> menus = sysMenuService.getAllMenus(0, 10);
        return new RespoBody(menus, meta);
    }

    @RequestMapping("/getRolesList")
    public RespoBody getRolesList() {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        List<SysRole> roles = sysUserService.getRoLeList();
        return new RespoBody(roles, meta);
    }

    @RequestMapping(value = "/delteRight/{roleId}/rightId/{rightid}", method = RequestMethod.DELETE)
    public RespoBody delteRight(@PathVariable int roleId, @PathVariable int rightid) {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        sysMenuService.deleteMrBymenuIdAndRoleId(roleId, rightid);
        List<SysMenu> sysMenus = sysUserService.getRoLeListByRoleId(roleId);
        return new RespoBody(sysMenus, meta);
    }

    /***
     * 分配权限
     * @param roleId
     * @param roleId
     * @return
     */
    @RequestMapping(value = "/saveRoleInfo/{userId}/roleId/{roleId}", method = RequestMethod.GET)
    public RespoBody saveRoleInfo(@PathVariable int userId, @PathVariable int roleId) {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        sysUserService.saveRoleInfo(userId, roleId);
        return new RespoBody(null, meta);
    }

    /***
     * 根据用户Id更新用户状态
     * @param userId
     * @param status
     * @return
     */
    @RequestMapping(value = "/updateUserStatus/{userId}/status/{status}", method = RequestMethod.GET)
    public RespoBody updateUserStatus(@PathVariable int userId, @PathVariable String status) {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        int upstatus = 0;
        if (status.equals("true")) {
            upstatus = 1;
        }
        sysUserService.updateUserStatus(userId, upstatus);
        return new RespoBody(null, meta);
    }

    /***
     *保存用户的基本信息
     * @param sysRole
     * @return
     */
    @PostMapping("/saveRole")
    public RespoBody saveRole(@RequestBody SysRole sysRole) {
        int i = sysUserService.saveRole(sysRole);
        Meta meta = new Meta();
        if (i > 0) {
            meta.setStatus(200);
            meta.setMsg("成功");
        } else {
            meta.setStatus(401);
            meta.setMsg("添加失败");
        }
        return new RespoBody(null, meta);
    }


    @RequestMapping("/getRoleById")
    @CrossOrigin(value = "http://localhost:8080", maxAge = 1800, allowedHeaders = "*")
    public RespoBody getRoleById(@RequestParam("roleId") int roleId) {
        SysRole role = sysUserService.findRoleById(roleId);
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        return new RespoBody(role, meta);
    }

    /***
     * 修改角色信息
     * @param sysRole
     * @return
     */
    @PostMapping("/editRole")
    public RespoBody editRole(@RequestBody SysRole sysRole) {
        int i = sysUserService.editRole(sysRole);
        Meta meta = new Meta();
        if (i > 0) {
            meta.setStatus(200);
            meta.setMsg("成功");
        } else {
            meta.setStatus(401);
            meta.setMsg("修改失败");
        }
        return new RespoBody(null, meta);
    }


    @RequestMapping("/getMenuList")
    public RespoBody getMenuList() {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        List<SysMenu> sysMenus = sysMenuService.getMunuList();
        //int count=productService.selectProductCountByParams(query);
        meta.setCount(10);
        return new RespoBody(sysMenus, meta);
    }


    /***
     *保存菜单信息
     * @param sysMenu
     * @return
     */
    @PostMapping("/saveMunu")
    public RespoBody saveRole(@RequestBody SysMenu sysMenu) {
        int i = sysMenuService.saveMenu(sysMenu);
        Meta meta = new Meta();
        if (i > 0) {
            meta.setStatus(200);
            meta.setMsg("成功");
        } else {
            meta.setStatus(401);
            meta.setMsg("添加失败");
        }
        return new RespoBody(null, meta);
    }


    @RequestMapping("/getUMenusById")
    public RespoBody getUMenusById(@RequestParam("id") int id) {
        SysMenu sysMenu = sysMenuService.getUMenusById(id);
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        return new RespoBody(sysMenu, meta);
    }

    /***
     * 根据menuId更新菜单信息
     * @param sysMenu
     * @return
     */
    @PostMapping("/editMenu")
    public RespoBody editMenu(@RequestBody SysMenu sysMenu) {
        int i = sysMenuService.updateMenu(sysMenu);
        Meta meta = new Meta();
        if (i > 0) {
            meta.setStatus(200);
            meta.setMsg("成功");
        } else {
            meta.setStatus(401);
            meta.setMsg("修改失败");
        }
        return new RespoBody(null, meta);
    }


    /***
     * 分配权限给角色
     * @return
     */
    @RequestMapping(value = "/saveRoleMenuInfo", method = RequestMethod.GET)
    public RespoBody saveRoleMenuInfo(@RequestParam int roleId, @RequestParam String menuIds) {
        Meta meta = new Meta();
        meta.setStatus(200);
        meta.setMsg("成功");
        sysUserService.saveRoleMenu(roleId, menuIds);
        return new RespoBody(null, meta);
    }
}
