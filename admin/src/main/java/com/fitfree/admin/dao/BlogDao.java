package com.fitfree.admin.dao;

import com.fitfree.admin.model.Blog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/***
 com.fitfree.admin.dao
 hanzl
 2020/5/1
 17:19
 **/
@Mapper
public interface BlogDao {
    List<Blog> queryList();
    int insert(Blog blog);
}
