package com.fitfree.admin.controller;

import com.fitfree.admin.utils.jwt.JwtUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/9
 * Time:16:23
 */
@RestController
public class BasicController  {
    @Autowired
    private HttpServletRequest myHttpRequest;
    @Autowired
    private HttpServletResponse myHttpResponse;
    public long getUserIdFromToken(){
        long userId=0;
        String token = myHttpRequest.getHeader("Token");
        if(StringUtils.isNotBlank(token)){
            userId= JwtUtil.getUserId(token);
        }
        return userId;

    }
}
