package com.fitfree.admin.controller;

import com.fitfree.admin.model.Blog;
import com.fitfree.admin.model.Category;
import com.fitfree.admin.service.BlogService;
import com.fitfree.admin.utils.Meta;
import com.fitfree.admin.utils.RespoBody;
import com.fitfree.admin.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/***
 com.fitfree.admin.controller
 hanzl
 2020/5/1
 17:28
 **/
@RestController
@RequestMapping("/blog")
public class BlogController {
    @Resource
    private BlogService blogService;

    /**
     * 获取博客列表
     *
     * @return
     */
    @RequestMapping("/list")
    public Result list() {
        List<Blog> list = blogService.queryList();
        return Result.success(list);
    }

    @PostMapping("/insert")
    public Result insert(@RequestBody Blog blog) {
        int i = blogService.insert(blog);
        if (i > 0) {
            return Result.success(i);
        } else {
            return Result.failure();
        }

    }
}
