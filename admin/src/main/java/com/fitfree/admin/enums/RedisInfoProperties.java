package com.fitfree.admin.enums;

/***
 com.fitfree.admin.enums
 hanzl
 2020/4/19
 21:44
 **/
public class RedisInfoProperties {
    public static final String server = "server";//有关Redis服务器的常规信息
    public static final String clients = "clients";//客户端连接部分
    public static final String memory = "memory";//内存消耗相关信息
    public static final String persistence = "persistence";//RDB和AOF相关信息
    public static final String stats = "stats";//一般统计
    public static final String replication = "replication";//主/副本复制信息
    public static final String cpu = "cpu";//CPU消耗统计信息
    public static final String commandstats = "commandstats";//Redis命令统计
    public static final String cluster = "cluster";// Redis群集”部分
    public static final String keyspace = "keyspace";//与数据库相关的统计
    public static final String all = "all";
}
