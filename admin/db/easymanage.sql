/*
Navicat MySQL Data Transfer

Source Server         : 本地库
Source Server Version : 80018
Source Host           : localhost:3306
Source Database       : easymanage

Target Server Type    : MYSQL
Target Server Version : 80018
File Encoding         : 65001

Date: 2020-05-01 23:10:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(255) DEFAULT NULL COMMENT '文章名称',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '文章内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新用户',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('1', '哈哈哈哈哈', null, null, null, null, null, null);
INSERT INTO `blog` VALUES ('2', '我的第一篇文章', '<h2 class=\"ql-indent-7\">我的第一篇文章</h2><ol><li class=\"ql-indent-7\">哈哈哈哈</li></ol><h3 class=\"ql-indent-7\">的男的女的</h3><p class=\"ql-indent-7\">多么多么多么的</p><p class=\"ql-indent-7\"><br></p><pre class=\"ql-syntax ql-indent-7\" spellcheck=\"false\">都看得到决定你的\n</pre>', '2020-05-01 21:15:35', null, null, null, null);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类名称',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父级编号',
  `category_level` tinyint(4) DEFAULT NULL COMMENT '类型（1:一级菜单,2:子级菜单,3:不是菜单）',
  `if_valid` tinyint(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('147', '11111111', '0', '1', '1');
INSERT INTO `category` VALUES ('148', '322322', '0', '1', '1');
INSERT INTO `category` VALUES ('149', '111111111', '147', '2', '1');
INSERT INTO `category` VALUES ('150', '111111111', '147', '2', '1');
INSERT INTO `category` VALUES ('151', '4444444', '0', '1', '1');
INSERT INTO `category` VALUES ('152', '一级2222', '0', '1', '1');
INSERT INTO `category` VALUES ('153', '耳机2222', '152', '2', '1');
INSERT INTO `category` VALUES ('154', '三级3333', '153', '3', '1');
INSERT INTO `category` VALUES ('155', '小米小米小米', '0', '1', '1');
INSERT INTO `category` VALUES ('156', '电器', '0', '1', '1');
INSERT INTO `category` VALUES ('157', '小米', '156', '2', '1');
INSERT INTO `category` VALUES ('158', '小米9', '157', '3', '1');

-- ----------------------------
-- Table structure for category_params
-- ----------------------------
DROP TABLE IF EXISTS `category_params`;
CREATE TABLE `category_params` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `category_id` bigint(20) DEFAULT NULL COMMENT '分类ID',
  `params_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `type` tinyint(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category_params
-- ----------------------------
INSERT INTO `category_params` VALUES ('147', '158', '长', '1');
INSERT INTO `category_params` VALUES ('148', '158', '20', '2');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `order_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `payment` tinyint(4) DEFAULT NULL COMMENT '支付方式',
  `if_pay` tinyint(4) DEFAULT NULL COMMENT '是否支付',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `category_id` bigint(20) DEFAULT NULL COMMENT '分类ID',
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `product_imageurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品图片',
  `product_num` bigint(20) DEFAULT NULL COMMENT '商品数量',
  `product_price` decimal(18,2) DEFAULT NULL COMMENT '商品价格',
  `product_params` varchar(255) DEFAULT NULL COMMENT '商品参数',
  `product_desc` varchar(255) DEFAULT NULL COMMENT '商品描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('147', '158', '小米9', '', '0', '97.00', '[object Object] [object Object]', '<p>11111111111111111111111111</p>', '2020-05-01 17:08:26');
INSERT INTO `product` VALUES ('148', '158', '小米9', '', '0', '97.00', '[object Object] [object Object] [object Object] [object Object]', '<p>11111111111111111111111111</p>', '2020-05-01 17:08:33');
INSERT INTO `product` VALUES ('149', '158', '小米9', '', '0', '97.00', '[object Object] [object Object] [object Object] [object Object] [object Object] [object Object]', '<p>11111111111111111111111111</p>', '2020-05-01 17:08:33');
INSERT INTO `product` VALUES ('150', '158', '小米9', '', '0', '97.00', '[object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object]', '<p>11111111111111111111111111</p>', '2020-05-01 17:08:33');
INSERT INTO `product` VALUES ('151', '158', '小米9', '', '0', '97.00', '[object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object]', '<p>11111111111111111111111111</p>', '2020-05-01 17:08:33');
INSERT INTO `product` VALUES ('152', '158', '小米9', '', '0', '97.00', '[object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object]', '<p>11111111111111111111111111</p>', '2020-05-01 17:08:33');
INSERT INTO `product` VALUES ('153', '158', '小米9', '', '0', '97.00', '[object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object]', '<p>11111111111111111111111111</p>', '2020-05-01 17:08:34');
INSERT INTO `product` VALUES ('154', '158', '小米9', '', '0', '97.00', '[object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object] [object Object]', '<p>11111111111111111111111111</p>', '2020-05-01 17:08:49');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父级编号',
  `parent_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所有父级编号',
  `url` varchar(255) DEFAULT NULL COMMENT 'URL地址',
  `perms` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `type` tinyint(4) DEFAULT NULL COMMENT '类型（1:一级菜单,2:子级菜单,3:不是菜单）',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新用户',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', null, null, null, 'el-icon-setting', '1', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('2', '用户管理', '1', null, 'user', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('3', '权限管理', '1', null, 'right', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('4', '角色管理', '1', null, 'roles', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('5', '商品管理', '0', null, null, null, 'el-icon-goods', '1', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('6', '商品分类', '5', null, 'category', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('7', '参数列表', '5', null, 'params', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('8', '商品列表', '5', null, 'goods', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('9', '订单管理', '5', null, 'orders', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('10', '报表管理', '5', null, 'report', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('11', '菜单管理', '1', null, 'menus', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('12', 'redis管理', '1', null, 'redisInfo', null, null, '2', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('147', '菜单', '0', null, '', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('148', '二级菜单', '147', null, 'yrryry', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('149', 'csd11', '0', null, '1', null, null, '1', null, null, null, null, null, null, '1');
INSERT INTO `sys_menu` VALUES ('150', '图标列表', '1', null, 'menuIcon', null, null, '2', null, null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('151', '加载', '5', null, '', null, 'el-icon-loading', '2', null, null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('152', '加载', '0', null, '', null, 'el-icon-loading', '1', null, null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('153', '博客', '0', null, '', null, 'el-icon-folder', '1', null, null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('154', '博客列表', '153', null, 'blog', null, 'el-icon-folder', '2', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `title` varchar(255) DEFAULT NULL COMMENT '角色名称（中文名）',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标识名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新用户',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', null, null, '管理员', null, null, null, null, null, null);
INSERT INTO `sys_role` VALUES ('4', null, null, '管理呀', '管理eeee', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '5');
INSERT INTO `sys_role_menu` VALUES ('1', '6');
INSERT INTO `sys_role_menu` VALUES ('1', '7');
INSERT INTO `sys_role_menu` VALUES ('1', '8');
INSERT INTO `sys_role_menu` VALUES ('1', '10');
INSERT INTO `sys_role_menu` VALUES ('1', '11');
INSERT INTO `sys_role_menu` VALUES ('1', '12');
INSERT INTO `sys_role_menu` VALUES ('1', '150');
INSERT INTO `sys_role_menu` VALUES ('1', '151');
INSERT INTO `sys_role_menu` VALUES ('1', '152');
INSERT INTO `sys_role_menu` VALUES ('1', '153');
INSERT INTO `sys_role_menu` VALUES ('1', '154');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `nickname` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `password` char(64) DEFAULT NULL COMMENT '密码',
  `salt` varchar(255) DEFAULT NULL COMMENT '密码盐',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `picture` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` tinyint(4) DEFAULT NULL COMMENT '性别（1:男,2:女）',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话号码',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '我是谁', '123', null, null, null, null, null, null, null, null, '1');
INSERT INTO `sys_user` VALUES ('3', 'hanzl', null, '123456', null, null, null, null, '123@qq.com', '15801054280', null, null, '1');
INSERT INTO `sys_user` VALUES ('4', 'hanzl', 'hanzl4444', '123456', null, null, null, null, '12345566@qq.com', '15801054280', null, null, '1');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('3', '1');
INSERT INTO `sys_user_role` VALUES ('4', '1');
