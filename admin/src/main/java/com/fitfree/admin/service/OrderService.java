package com.fitfree.admin.service;

import com.fitfree.admin.dao.OrderDao;
import com.fitfree.admin.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/28
 * Time:8:15
 */
@Service
public class OrderService {
    @Autowired
    private OrderDao orderDao;
    public List<Order> getOrderList(String query, int pageNo, int pageSize){
        if (pageNo == 0) {
            pageNo = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        pageNo = (pageNo - 1) * pageSize;
        return orderDao.getOrderList(query, pageNo, pageSize);
    }

    /**
     * 查询订单数量
     * @param query
     * @return
     */
    public int selectOrdersCountByParams(String query){
        return orderDao.selectOrdersCountByParams(query);
    }
}
