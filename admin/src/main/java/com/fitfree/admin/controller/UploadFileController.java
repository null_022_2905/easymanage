package com.fitfree.admin.controller;

import com.fitfree.admin.utils.Meta;
import com.fitfree.admin.utils.RespoBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IDEA
 * author:hanzhenlong
 * Date:2020/2/20
 * Time:12:39
 */
@RestController
@RequestMapping(value = "/")
public class UploadFileController {
    /**
     * 接收上传的文件，并且将上传的文件存储在指定路径下
     * @return
     */
    @RequestMapping(value = "/upload")
    public RespoBody upload(@RequestParam("file")MultipartFile srcFile ) {
        InputStream is = null;
        FileOutputStream fos = null;
        try {
            // 文件名
            String filename = srcFile.getOriginalFilename();
            // 存储路径
            String filePath = "e:/myimage";
            File file = new File(filePath+"/"+filename);
            if(!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if(!file.exists()) {
                file.createNewFile();
            }

            is = srcFile.getInputStream();
            fos = new FileOutputStream(file);
            byte[] content = new byte[1024];
            int len = 0;
            while((len=is.read(content)) > -1) {
                fos.write(content, 0, len);
            }
            fos.flush();

            Meta meta=new Meta();
            meta.setStatus(200);
            String imagePath="http://localhost:8083/file/"+filename;
           return new RespoBody(imagePath,meta);
        } catch (Exception ex) {
            ex.printStackTrace();
            Meta meta=new Meta();
            meta.setStatus(201);
            return new RespoBody(null,meta);
        } finally {
            try {
                if(is!=null) {
                    is.close();
                }
                if(fos!=null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

